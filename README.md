# myNewPackage

This is an example package for the EpiHub Coding Club 2024-04-08,
containing the two functions: `plotFeatures()` and `makeLMs` that 
we walked through during the session.  

I hope it makes sense - and don't hesitate to ask if there's anything unclear!  

Best wishes,  
Carl
